import django_filters
from rest_framework import generics

from .models import Post

from .serializers import PostSerializer, PostSerializerDetail


class BasePostView:
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    lookup_field = 'id'


class PostListView(BasePostView, generics.ListCreateAPIView):
    filter_backends = (django_filters.rest_framework.DjangoFilterBackend,)
    filterset_fields = ('author',)

    def perform_create(self, serializer):
        serializer.validated_data.update({'author': self.request.user})
        super().perform_create(serializer)


class PostDetailView(BasePostView, generics.RetrieveAPIView):
    serializer_class = PostSerializerDetail


class PostLikeView(BasePostView, generics.RetrieveAPIView):
    def get(self, request, *args, **kwargs):
        instance: Post = self.get_object()
        instance.likes += 1
        instance.save()
        return super().get(request, *args, **kwargs)


class PostDislikeView(BasePostView, generics.RetrieveAPIView):
    def get(self, request, *args, **kwargs):
        instance: Post = self.get_object()
        instance.dislikes += 1
        instance.save()
        return super().get(request, *args, **kwargs)
