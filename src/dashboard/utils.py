import json
import os
import random

import faker
from django.conf import settings
from django.contrib.auth.models import User

from .models import Post

BOT_CONFIG_DIR_NAME = 'bot_configs'
CSV_DUMPS_DIR = os.path.join(settings.BASE_DIR, '..', BOT_CONFIG_DIR_NAME)

fake = faker.Faker()


def _user_factory() -> User:
    new_user = User.objects.create_user(
        username=fake.name(),
        email=fake.email(),
        password=fake.password()
    )
    print(f'CREATED: {new_user}')
    return new_user


def _post_factory(user: User, max_likes: int, max_dislikes: int) -> None:
    Post.objects.create(
        title=fake.text(max_nb_chars=50),
        content=fake.text(max_nb_chars=1000),
        date_created=fake.date_time_between(start_date='-7y', end_date='now'),
        author=user,
        likes=random.randint(0, max_likes),
        dislikes=random.randint(0, max_dislikes)
    )


class ContentBot:
    def __init__(self, config: str):
        if not config or config == '':
            raise ValueError('config filename is not set')

        full_path = os.path.join(CSV_DUMPS_DIR, config)

        if not os.path.isfile(full_path):
            raise FileExistsError(f'file does not exist: {full_path}')

        with open(full_path, 'rt') as target_file:
            self.config_file = json.loads(target_file.read())

    def config_applied(self) -> bool:
        """
        todo validation for config
        basic validation for config file
        """

        return self.config_file is not None and len(self.config_file)

    def create_fake_content(self) -> None:
        """presentation content generator"""

        self._create_users()
        print('now the process of generating posts for users...')

        self._create_user_posts()
        print('all posts are added!')

    def _create_users(self) -> None:
        self.users = tuple(_user_factory() for _ in range(self.config_file['number_of_users']))

    def _create_user_posts(self) -> None:
        posts_count = self.config_file['max_posts_per_user']
        m_likes = self.config_file['max_likes_per_user']
        m_dis = self.config_file['max_dislikes_per_user']

        [_post_factory(user, m_likes, m_dis) for user in self.users for _ in range(random.randint(0, posts_count))]
