from rest_framework import serializers

from .models import Post, CONTENT_MAX_LENGTH, TITLE_MAX_LENGTH


class PostSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='post_details_url', lookup_field='id')

    title = serializers.CharField(max_length=TITLE_MAX_LENGTH)
    short_content = serializers.SerializerMethodField()
    content = serializers.CharField(
        max_length=CONTENT_MAX_LENGTH,
        write_only=True,
        style={'base_template': 'textarea.html'}
    )

    date_created = serializers.DateTimeField(allow_null=True, format='%Y-%m-%d %H:%M')
    author = serializers.CharField(read_only=True)

    likes = serializers.ReadOnlyField()
    dislikes = serializers.ReadOnlyField()

    class Meta:
        model = Post
        fields = (
            'url',
            'title',
            'author',
            'short_content',
            'content',
            'date_created',
            'likes',
            'dislikes'
        )

    @staticmethod
    def get_short_content(post_obj: Post) -> str:
        return f'{post_obj.content[:15]}...' if len(post_obj.content) > 15 else post_obj.content


class PostSerializerDetail(PostSerializer):
    content = serializers.CharField()

    like_post = serializers.HyperlinkedIdentityField(view_name='post_like_url', lookup_field='id')
    dislikes_post = serializers.HyperlinkedIdentityField(view_name='post_dislike_url', lookup_field='id')

    class Meta:
        model = Post
        fields = (
            'title',
            'author',
            'content',
            'date_created',
            'likes',
            'dislikes',
            'like_post',
            'dislikes_post'
        )
