from django.contrib.auth.models import User
from django.db import models

CONTENT_MAX_LENGTH = 5000
TITLE_MAX_LENGTH = 200


class Post(models.Model):
    title = models.CharField(max_length=TITLE_MAX_LENGTH, db_index=True)
    content = models.TextField(max_length=CONTENT_MAX_LENGTH)
    date_created = models.DateTimeField(auto_now_add=True, blank=True, db_index=True)

    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name='posts')

    likes = models.IntegerField(default=0)
    dislikes = models.IntegerField(default=0)

    class Meta:
        ordering = ['-date_created']

    def __str__(self):
        return f'"{self.title}" by {self.author}'
