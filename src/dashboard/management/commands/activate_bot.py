import importlib

from django.contrib.auth.models import User
from django.core.management import BaseCommand, CommandError

default_config = 'config_test.json'
dashboard_utils = importlib.import_module('dashboard.utils')  # explicit import the models module


class Command(BaseCommand):
    help = 'command to activate bot and add some mock-data' \
           'first arg will be pass like a file_name. the base dir is PROJECT_ROOT/bot_config'

    def add_arguments(self, parser):
        parser.add_argument(
            *('--config', '-C'),
            help='bot config filename. file format *.json',
            nargs='?',
            type=str,
            default=default_config,
        )
        parser.add_argument(
            *('--delete', '-D'),
            help='0 means do not delete existing users, 1 is delete. default 1',
            choices=[0, 1],
            type=int,
            default=1,
        )

    def handle(self, *args, **options):
        try:
            delete_users = options['delete']

            if delete_users != 0 and delete_users == 1:
                existing_users = User.objects.all()
                existing_users.delete() if existing_users.count() > 0 else print('no users detected\n')

            config = options['config']
            content_bot = dashboard_utils.ContentBot(config=config)

            if content_bot.config_applied():
                print('start bot process...')
                content_bot.create_fake_content()
                print('bot job to create some presentation data successfully done')
            else:
                print('some problem while applying configuration')

        except Exception as e:
            raise CommandError(repr(e))
