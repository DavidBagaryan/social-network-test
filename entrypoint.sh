#!/bin/sh

cd ./src/

./manage.py makemigrations
./manage.py migrate

./manage.py activate_bot

echo 'from django.contrib.auth.models import User;' \
     'User.objects.filter(email="admin@example.com").delete();' \
     'User.objects.create_superuser("admin", "admin@example.com", "admin666")' | ./manage.py shell

exec "$@"
