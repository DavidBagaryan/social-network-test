FROM python:3.7-alpine

WORKDIR /usr/src/app

RUN pip install --upgrade pip
RUN pip install pipenv
COPY ./ /usr/src/app/
RUN pipenv install --skip-lock --system --dev

# run entrypoint.sh
ENTRYPOINT ["/usr/src/app/entrypoint.sh"]
